// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$ProductStore on _ProductStoreBase, Store {
  late final _$productAtom =
      Atom(name: '_ProductStoreBase.product', context: context);

  @override
  ProductModel get product {
    _$productAtom.reportRead();
    return super.product;
  }

  @override
  set product(ProductModel value) {
    _$productAtom.reportWrite(value, super.product, () {
      super.product = value;
    });
  }

  late final _$productStateAtom =
      Atom(name: '_ProductStoreBase.productState', context: context);

  @override
  ProductStateEnum get productState {
    _$productStateAtom.reportRead();
    return super.productState;
  }

  @override
  set productState(ProductStateEnum value) {
    _$productStateAtom.reportWrite(value, super.productState, () {
      super.productState = value;
    });
  }

  late final _$getProductAsyncAction =
      AsyncAction('_ProductStoreBase.getProduct', context: context);

  @override
  Future<void> getProduct(String qrCode) {
    return _$getProductAsyncAction.run(() => super.getProduct(qrCode));
  }

  @override
  String toString() {
    return '''
product: ${product},
productState: ${productState}
    ''';
  }
}
