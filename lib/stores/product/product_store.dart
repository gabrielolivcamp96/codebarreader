import 'package:get_it/get_it.dart';
import 'package:mobx/mobx.dart';

import '../../models/product_model.dart';
import '../../services/product_services.dart';
import '../../utils/enum/product_state_enum.dart';
import '../user/user_store.dart';
part 'product_store.g.dart';

class ProductStore = _ProductStoreBase with _$ProductStore;

abstract class _ProductStoreBase with Store {
  // Só pra receber os dados
  @observable
  ProductModel product =
      ProductModel(id: 1, name: 'name', price1: 0.0, price2: 0.0, price3: 0.0);

  @observable
  ProductStateEnum productState = ProductStateEnum.start;

  @action
  Future<void> getProduct(String qrCode) async {
    productState = ProductStateEnum.loading;
    // Map<String, dynamic> data = {
    //   'code': qrCode,
    //   'filter': "da1_filial eq '010105'"
    // };
    // var item = await ProductServices()
    //     .getProduct(data, GetIt.I.get<UserStore>().token);

    // product = ProductModel.fromJson(item);
    product.name = qrCode;
    productState = ProductStateEnum.sucess;
  }
}
