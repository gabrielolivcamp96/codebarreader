// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'qr_code_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$QrCodeStore on _QrCodeStoreBase, Store {
  late final _$qrCodeAtom =
      Atom(name: '_QrCodeStoreBase.qrCode', context: context);

  @override
  String get qrCode {
    _$qrCodeAtom.reportRead();
    return super.qrCode;
  }

  @override
  set qrCode(String value) {
    _$qrCodeAtom.reportWrite(value, super.qrCode, () {
      super.qrCode = value;
    });
  }

  late final _$_QrCodeStoreBaseActionController =
      ActionController(name: '_QrCodeStoreBase', context: context);

  @override
  dynamic sendToPage(String qrCode, BuildContext context) {
    final _$actionInfo = _$_QrCodeStoreBaseActionController.startAction(
        name: '_QrCodeStoreBase.sendToPage');
    try {
      return super.sendToPage(qrCode, context);
    } finally {
      _$_QrCodeStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
qrCode: ${qrCode}
    ''';
  }
}
