import 'package:codebarreader/pages/product_page.dart';
import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';
part 'qr_code_store.g.dart';

class QrCodeStore = _QrCodeStoreBase with _$QrCodeStore;

abstract class _QrCodeStoreBase with Store {
  // TODO logica da impl do qr code

  @observable
  String qrCode = '';

  @action
  sendToPage(String qrCode, BuildContext context) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => ProductPage(qrCode: qrCode),
      ),
    );
  }
}
