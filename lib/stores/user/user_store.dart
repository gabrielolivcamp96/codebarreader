import 'package:mobx/mobx.dart';

import '../../services/login_services.dart';
part 'user_store.g.dart';

class UserStore = _UserStoreBase with _$UserStore;

abstract class _UserStoreBase with Store {
  // TODO logica do usuario para token
  @observable
  String password = '';

  @observable
  String token = '';

  @action
  Future<void> login(String password) async {
    // var response = await LoginServices().login(password);

    token = "response.data['acess_token']";
  }
}
