import 'package:dio/dio.dart';

class ProductServices {
  Dio dio = Dio();

  getProduct(Map<String, dynamic> data, token) async {
    try {
      Response response = await dio.post(
          'https://192.168.2.158:28400/rest/api/v1/produtos/precos',
          queryParameters: data,
          options: Options(
            headers: {"Authorization": 'Bearer $token'},
          ));

      return response;
    } on DioException catch (e) {
      // TODO
      print('object');
      rethrow;
    }
  }
}
