import 'dart:convert';

// ignore_for_file: public_member_api_docs, sort_constructors_first
class ProductModel {
  int id;
  String name;
  double price1;
  double price2;
  double price3;
  ProductModel({
    required this.id,
    required this.name,
    required this.price1,
    required this.price2,
    required this.price3,
  });

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'id': id,
      'name': name,
      'price1': price1,
      'price2': price2,
      'price3': price3,
    };
  }

  factory ProductModel.fromMap(Map<String, dynamic> map) {
    return ProductModel(
      id: map['da1_codpro'] as int,
      name: map['b1_desc'] as String,
      price1: map['price1'] as double,
      price2: map['price2'] as double,
      price3: map['price3'] as double,
    );
  }

  String toJson() => json.encode(toMap());

  factory ProductModel.fromJson(String source) =>
      ProductModel.fromMap(json.decode(source) as Map<String, dynamic>);
}
