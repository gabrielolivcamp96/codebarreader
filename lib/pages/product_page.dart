// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:codebarreader/utils/enum/product_state_enum.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:codebarreader/stores/product/product_store.dart';

// ignore: must_be_immutable
class ProductPage extends StatefulWidget {
  String qrCode;

  ProductPage({
    Key? key,
    required this.qrCode,
  }) : super(key: key);

  @override
  State<ProductPage> createState() => _ProductPageState();
}

class _ProductPageState extends State<ProductPage> {
  final productStore = ProductStore();

  @override
  void initState() {
    // TODO: implement initState
    productStore.getProduct(widget.qrCode);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Observer(
        builder: (context) {
          return productStore.productState != ProductStateEnum.loading
              ? Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text('Nome do produto: ${productStore.product.name}'),
                      Text('Preço:${productStore.product.price1}'),
                      Text('Preço:${productStore.product.price2}'),
                      Text('Preço:${productStore.product.price3}'),
                    ],
                  ),
                )
              : const Center(
                  child: CircularProgressIndicator(),
                );
        },
      ),
    );
  }
}
