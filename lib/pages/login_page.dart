import 'package:codebarreader/pages/qr_reader_page.dart';
import 'package:codebarreader/pages/testeqr.dart';
import 'package:codebarreader/stores/user/user_store.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final userStore = GetIt.I.get<UserStore>();
  TextEditingController password = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextFormField(
              controller: password,
              onChanged: (value) {
                password.text = value;
              },
            ),
            // TextFormField(),
            ElevatedButton(
              onPressed: () async {
                await userStore.login(password.text);

                if (userStore.token.isNotEmpty) {
                  if (!context.mounted) return;
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => const BarcodeScannerWithOverlay(),
                    ),
                  );
                }
              },
              child: const Text("Acessar"),
            ),
          ],
        ),
      ),
    );
  }
}
