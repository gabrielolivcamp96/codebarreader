// import 'package:flutter/material.dart';
// import 'package:mobile_scanner/mobile_scanner.dart';

// import 'product_page.dart';

// class QrReaderPage extends StatefulWidget {
//   const QrReaderPage({super.key});

//   @override
//   State<QrReaderPage> createState() => _QrReaderPageState();
// }

// class _QrReaderPageState extends State<QrReaderPage>
//     with SingleTickerProviderStateMixin {
//   String overlayText = "Please scan QR Code";
//   bool camStarted = false;

//   final MobileScannerController controller = MobileScannerController(
//     // formats: const [BarcodeFormat.qrCode],
//     detectionTimeoutMs: 1000,
//     autoStart: true,
//   );

//   @override
//   void dispose() {
//     controller.dispose();
//     super.dispose();
//   }

//   void onBarcodeDetect(BarcodeCapture barcodeCapture) {
//     final barcode = barcodeCapture.barcodes.last;
//     Navigator.of(context).push(
//       MaterialPageRoute(
//         builder: (context) => ProductPage(qrCode: barcode.rawValue!),
//       ),
//     );
//     setState(() {
//       overlayText = barcodeCapture.barcodes.last.displayValue ??
//           barcode.rawValue ??
//           'Barcode has no displayable value';
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     final scanWindow = Rect.fromCenter(
//       center: MediaQuery.of(context).size.center(Offset.zero),
//       width: 350,
//       height: 200,
//     );

//     return Scaffold(
//       appBar: AppBar(
//         title: const Text('Scanner with Overlay Example app'),
//       ),
//       body: Center(
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: <Widget>[
//             Expanded(
//               child:
//                   //  camStarted
//                   //     ?
//                   Stack(
//                 fit: StackFit.expand,
//                 children: [
//                   Center(
//                     child: MobileScanner(
//                       fit: BoxFit.contain,
//                       onDetect: onBarcodeDetect,

//                       overlayBuilder: (context, constraints) {
//                         return Padding(
//                           padding: const EdgeInsets.all(16.0),
//                           child: Align(
//                             alignment: Alignment.bottomCenter,
//                             child: Opacity(
//                               opacity: 0.7,
//                               child: Text(
//                                 overlayText,
//                                 style: const TextStyle(
//                                   backgroundColor: Colors.black26,
//                                   color: Colors.white,
//                                   fontWeight: FontWeight.bold,
//                                   fontSize: 24,
//                                   overflow: TextOverflow.ellipsis,
//                                 ),
//                                 maxLines: 1,
//                               ),
//                             ),
//                           ),
//                         );
//                       },
//                       controller: controller,
//                       scanWindow: scanWindow,
//                       // errorBuilder: (context, error, child) {
//                       //   // return ScannerErrorWidget(error: error);
//                       // },
//                     ),
//                   ),
//                   CustomPaint(
//                     painter: ScannerOverlay(scanWindow),
//                   ),
//                   Padding(
//                     padding: const EdgeInsets.all(16.0),
//                     child: Align(
//                       alignment: Alignment.bottomCenter,
//                       child: Row(
//                         mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                         children: [
//                           ValueListenableBuilder<TorchState>(
//                             valueListenable: controller.torchEnabled,
//                             builder: (context, value, child) {
//                               final Color iconColor;

//                               switch (value) {
//                                 case TorchState.off:
//                                   iconColor = Colors.black;
//                                   break;
//                                 case TorchState.on:
//                                   iconColor = Colors.yellow;
//                                   break;
//                               }

//                               return IconButton(
//                                 onPressed: () => controller.toggleTorch(),
//                                 icon: Icon(
//                                   Icons.flashlight_on,
//                                   color: iconColor,
//                                 ),
//                               );
//                             },
//                           ),
//                           IconButton(
//                             onPressed: () => controller.switchCamera(),
//                             icon: const Icon(
//                               Icons.cameraswitch_rounded,
//                               color: Colors.white,
//                             ),
//                           ),
//                         ],
//                       ),
//                     ),
//                   ),
//                 ],
//               ),
//               // : const Center(
//               //     child: Text("Tap on Camera to activate QR Scanner"),
//               //   ),
//             ),
//           ],
//         ),
//       ),
//       // floatingActionButton: camStarted
//       //     ? null
//       //     : FloatingActionButton(
//       //         onPressed: startCamera,
//       //         child: const Icon(Icons.camera_alt),
//       //       ),
//     );
//   }
// }

// class ScannerOverlay extends CustomPainter {
//   ScannerOverlay(this.scanWindow);

//   final Rect scanWindow;
//   final double borderRadius = 12.0;

//   @override
//   void paint(Canvas canvas, Size size) {
//     final backgroundPath = Path()..addRect(Rect.largest);
//     final cutoutPath = Path()
//       ..addRRect(
//         RRect.fromRectAndCorners(
//           scanWindow,
//           topLeft: Radius.circular(borderRadius),
//           topRight: Radius.circular(borderRadius),
//           bottomLeft: Radius.circular(borderRadius),
//           bottomRight: Radius.circular(borderRadius),
//         ),
//       );

//     final backgroundPaint = Paint()
//       ..color = Colors.black.withOpacity(0.5)
//       ..style = PaintingStyle.fill
//       ..blendMode = BlendMode.dstOut;

//     final backgroundWithCutout = Path.combine(
//       PathOperation.difference,
//       backgroundPath,
//       cutoutPath,
//     );

//     // Create a Paint object for the white border
//     final borderPaint = Paint()
//       ..color = Colors.white
//       ..style = PaintingStyle.stroke
//       ..strokeWidth = 2.0; // Adjust the border width as needed

//     // Calculate the border rectangle with rounded corners
// // Adjust the radius as needed
//     final borderRect = RRect.fromRectAndCorners(
//       scanWindow,
//       topLeft: Radius.circular(borderRadius),
//       topRight: Radius.circular(borderRadius),
//       bottomLeft: Radius.circular(borderRadius),
//       bottomRight: Radius.circular(borderRadius),
//     );

//     // Draw the white border
//     canvas.drawPath(backgroundWithCutout, backgroundPaint);
//     canvas.drawRRect(borderRect, borderPaint);
//   }

//   @override
//   bool shouldRepaint(covariant CustomPainter oldDelegate) {
//     return false;
//   }
// }
